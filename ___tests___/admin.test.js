const puppeteer = require('puppeteer');
let browser;
let page;
const url='http://172.16.4.182:7500/';

//---------------------------------Admin can VIEW list of Employee-------------------------------ok-----//

// test('TC_admin _01 Admin can VIEW list of Employee', async() => {
//      browser = puppeteer.launch({headless: false, slowMo: 50})
//      page = (await browser).newPage()  
      
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
   
//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password') 
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//  //Wait for data to be displayed on table
//   await (await page).waitForSelector('.table.b-table.table-sm')
//     const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//     expect(html).toBeTruthy();
 
//     await (await browser).close()
// }, 300000)

//------------Admin can search a Employee----------ok--------//

// test('TC_admin_02 Admin can search car by (Employee number,Name, Role, Age, Phone, E-mail)', async() =>{
//     browser = puppeteer.launch({headless: false, slowMo: 100})
//    page = (await browser).newPage()
    
    
//     await (await page).goto(url)
//     await(await page).setViewport({ width: 2133, height: 1076 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)
 
//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Mechanic');
//     await (await page).waitForSelector('.table.b-table.table-sm')
//        const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//        expect(html).toBeTruthy();

//     // await (await page).goto('http://172.16.4.182:7500/adminmanage')
//     // await (await page).waitForSelector('.form-control')
//     await (await browser).close()
    
// }, 20000)
///---------------------- Admin cannot add new employee without complete name--------------Ok--------------//

// test('TC_admin_03 Validation for ADMIN cannot add new Employee/s without complete name', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[7].type('manayan@gmail.com')
//     await form[8].type('Darw')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Incomplete name.")
  
//   await browser.close()
// },50000 ) 

// ///---------------------- Admin cannot add new employee without Position-------------Ok-------------//

// test('TC_admin_04 Validation for ADMIN cannot add new Employee/s without Position', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Darwin')
//     await form[3].type('P')
//     await form[4].type('Manayan')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     await form[6].type('09097564511')
//     await form[7].type('manayan@gmail.com')
//     await form[8].type('Darw')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Position required.")
  
//   await browser.close()
// },50000 ) 

// ///---------------------- Admin cannot add new employee without Age------------OK----------//

// test('TC_admin_05 Validation for ADMIN cannot add new Employee/s without Position', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Darwin')
//     await form[3].type('P')
//     await form[4].type('Manayan')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[7].type('manayan@gmail.com')
//     await form[8].type('Darw')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Age required.")
  
//   await browser.close()
// },50000 ) 
//----------------------------------------ADMIN CANNOT ADD NEW CUSTOMER WITHOUT MOBILE NUMBER-----------------------------------//

// test('TC_admin_06 Validation for ADMIN cannot add new Employee/s without mobile number', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//   //log in as admin
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Love')
//     await form[3].type('E')
//     await form[4].type('Marie')
//      await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[7].type('love@gmail.com')
//     await form[8].type('Love')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Contact number requried.")
  
//   await browser.close()
// },50000 ) 

//--------------------------------------------ADMIN cannot add new employee without E-mail--------------------------------------//
// test('TC_admin_07 Validation for ADMIN cannot add new Employee/s without E-mail', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Jennie')
//     await form[3].type('H')
//     await form[4].type('Kim')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[8].type('Jen')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("E-mail required.")
  
//   await browser.close()
// },50000 ) 

// -------------------ADMIN cannot ADD new employee without Username------------------------////

// test('TC_admin_08 Validation for ADMIN cannot add new Employee/s without username', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Rose')
//     await form[3].type('M')
//     await form[4].type('Kim')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[7].type('rose@gmail.com')
//     await form[9].type('1234')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Username required.")
  
//   await browser.close()
// },50000 ) 

////------------------ADMIN cannot ADD new employee without password------------------------------///
// test('TC_admin_09 Validation for ADMIN cannot add new Employee/s without Position', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
    
//     await form[2].type('Kin')
//     await form[3].type('D')
//     await form[4].type('Gee')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[7].type('kin@gmail.com')
//     await form[8].type('Kin')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Password required.")
  
//   await browser.close()
// },50000 ) 

//----------Admin cannot add new employee without detail--------------//
// test('TC_admin_10 Validation for ADMIN cannot add new Employee/s without Details', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 50})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')

//     await page.waitForSelector('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
//     await page.click('#createEmployee___BV_modal_content_ > #createEmployee___BV_modal_body_ > div > form > .btn')
   
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Incomplete name.")
  
//   await browser.close()
// },50000 ) 

///---------------------- Admin can add new employee with complete Details-------------------ok-------------//
// test('TC_admin_11 Validation for ADMIN can add new Employee/s', async() => {
//     const browser = await puppeteer.launch({headless: false, slowMo: 100})
//     const page = await browser.newPage()

//     await (await page).goto(url)
//     await(await page).setViewport({ width: 2133, height: 1076 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await page.waitForSelector('.btn.btn-primary')
//     await page.click('.btn.btn-primary')

//     const form = await page.$$('.form-control')
//     //
//     await form[2].type('Darwin')
//     await form[3].type('P')
//     await form[4].type('Manayan')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Mechanic')
//     await form[6].type('09097564511')
//     await form[7].type('manayan@gmail.com')
//     await form[8].type('Darw')
//     await form[9].type('1234')

//     await page.click('.btn.btn-primary')

//     await page.waitForSelector('http://172.16.4.182:7500/adminmanage')
  
//   await browser.close()
// },50000 ) 


// //  -----------------------------Admin can update employee details------dili pa-----------//

// test('TC_admin_12 Admin can update employee details', async() => {
// const browser = await puppeteer.launch({headless: false, slowMo: 100})
// const page = await browser.newPage()

// await (await page).goto(url)
// await(await page).setViewport({ width: 2133, height: 1076 })

// await (await page).waitForSelector('.username')
// await (await page).click('.username')

// await (await page).type('.username','admin') //ibutang niya ang ID

// await (await page).waitForSelector('.password')
// await (await page).click('.password')

// await (await page).type('.password', '1234')
// await (await page).click('.btn.login')
// await (await page).waitForSelector('#filterInput')
// await (await page).click('#filterInput')
// await (await page).type('#filterInput', 'Mechanic');

// await page.waitForSelector('.btn.mr-1')
// await page.click('.btn.mr-1')
// const form = await page.$$('.form-control')
//     await form[2].click( { clickCount: 3 }) // i clear niya ang content
//     await form[2].type('Darwin')

//   await form[2].type('Darwin')
//     await form[3].type('P')
//     await form[4].type('Manayan')
//     await form[5].type('21')   
//     await page.click('.mb-2')
//     const role = await page.$('.mb-2')  /// click and may dropdown
//     await role.type('Admin')
//     await form[6].type('09097564511')
//     await form[7].type('manayan@gmail.com')
//     await form[8].type('Darw')
//     await form[9].type('1234')

//     await page.click('.btn.btn-primary')
   
//     await page.waitForSelector('#app > .wrapper > .page-header > div > .alert')
//     const data = await page.evaluate(() => {
//         let Invalid = document.querySelector('.alert').innerText;
//         return Invalid;
//       })
//       expect(data).toMatch("Position required.")

//     await browser.close();
// }, 40000)

// -----------------------------------------------------------CAR MODULE-----------------------------------------------------------

//---------- Admin can view car list ----------Ok---//
//  test ('TC_admin_13 Admin can view car list', async() => {
//       const browser = await puppeteer.launch({headless: false, slowMo: 100})
//       const page = await browser.newPage()

//       await (await page).goto(url)
//       await(await page).setViewport({ width: 2133, height: 1076 })

//       await (await page).waitForSelector('.username')
//       await (await page).click('.username')

//       await (await page).type('.username','admin') //ibutang niya ang ID

//       await (await page).waitForSelector('.password')
//       await (await page).click('.password')

//       await (await page).type('.password', '1234')
//       await (await page).click('.btn.login')

//       await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//       await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//     //   await page.waitForSelector('btn btn-primary');
//     //  await page.click('btn btn-primary')
//       await (await page).waitForSelector('.table.b-table.table-sm')
//           const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//           expect(html).toBeTruthy();
//      await browser.close();
//  }, 200000)

//--------------- Admin cannot add new car without serial number ----OK-------------///
// test('TC_admin_14 Admin cannot add new car without serial number', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//     //click add car
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')

//      //Add car form
//     const form = await page.$$('.form-control')

//      await form[2].type('Toyota')
//      await form[3].type('Hilux')
//      await form[4].type('100000')
//      await form[5].type('Pink')
//          const down = await page.$$('.mb-2')
//           await page.click('.mb-2')
//                 await down[0].type('New')
//      await form[6].type('70-horse power')
//                 await down[1].type('Manual')
//      await form[7].type('Yes')
//      await down[2].type('Diesel')

//      //Create button
//      await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//      await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//      //Alert
//      await page.waitForSelector('#app > .wrapper > div > div > .alert')
//      await page.click('#app > .wrapper > div > div > .alert')

//      //Assertion
//      const data = await page.evaluate(() => {
//            let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                        return Invalid;})
//                        expect(data).toMatch("VIN Required.")

//            await (await browser).close()
//         }, 80000)

//--------------- Admin cannot add new car without Brand ----------Ok----///
// test('TC_Admin_15 Admin cannot add new car without Brand', async() => {

//     browser = puppeteer.launch({headless: false, slowMo: 50})
//     page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')


//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')


//     const form = await page.$$('.form-control')
//         await form[1].type('Hilux2010')
//         await form[3].type('Hilux')
//         await form[4].type('100000')
//         await form[5].type('Pink')
//             

//         const down = await page.$$('.mb-2')
//         await page.click('.mb-2')
//               await down[0].type('New')
//         await form[6].type('70-horse power')
//               await down[1].type('Manual')
//         await form[7].type('Yes')
//              await down[2].type('Diesel')

//         await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//         await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//         await page.waitForSelector('#app > .wrapper > div > div > .alert')
//         await page.click('#app > .wrapper > div > div > .alert')


//         const data = await page.evaluate(() => {
//               let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//               return Invalid;})
//               expect(data).toMatch("Company required.")
//          await (await browser).close()
//         }, 80000)

//--------------- Admin cannot add new car without Price ---------Ok----///
// test('TC_Admin_16 Admin cannot add new car without price', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('Hilux2010')
//         await form[2].type('Toyota')
//         await form[3].type('Hilux')
//         await form[5].type('Pink')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//                 await down[0].type('New')
//              await form[6].type('70-horse power')
//                 await down[1].type('Manual')

//             await form[7].type('Yes')
//             await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')


//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Car price required.")

//             await (await browser).close()
//             }, 80000)

//--------------- Admin cannot add new car without Color ----------Ok----///
// test('TC_Admin_17 Admin cannot add new car without Color', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('Hilux2010')
//         await form[2].type('Toyota')
//         await form[3].type('Hilux')
//         await form[4].type('100000')
//         // await form[5].type('Pink')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//                 await down[0].type('New')
//              await form[6].type('70-horse power')

//             // await page.click('#__BVID__1691')
//                 await down[1].type('Manual')

//             await form[7].type('Yes')

//             // await page.click('#__BVID__1693')
//              await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')


//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Car color required.")

//          await (await browser).close()
//         }, 80000)

//--------------- Admin cannot add new car without status ----------Ok----///
// test('TC_Admin_18 Admin cannot add new car without Status', async() => {

//     const browser = puppeteer.launch({headless: true, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')
//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('Hilux2010')
//         await form[2].type('Toyota')
//         await form[3].type('Hilux')
//         await form[4].type('100000')
//         await form[5].type('Pink')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//             //await down[0].type('New')
//         await form[6].type('70-horse power')
//              await down[1].type('Manual')
//          await form[7].type('Yes')
//              await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Car status required.")

//          await (await browser).close()
//         }, 80000)

//----------- ADMIN cannot add new car without horsepower-----------------------//
// test('TC_Admin_19 Admin cannot add new car without Horsepower', async() => {

//     const browser = puppeteer.launch({headless: true, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')
//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('HondaFit')
//         await form[2].type('Honda')
//         await form[3].type('Fit')
//         await form[4].type('100000')
//         await form[5].type('Black')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//             await down[0].type('New')
//              await down[1].type('Manual')
//          await form[7].type('Yes')
//              await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Horsepower required.")
//          await (await browser).close()
//         }, 80000)

//-----cannot add new car without Transmission-------------------------//
// test('TC_Admin_20 Admin cannot add new car without Transmission', async() => {
//     const browser = puppeteer.launch({headless: true, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')
//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('HondaCRV')
//         await form[2].type('Honda')
//         await form[3].type('CRV')
//         await form[4].type('100000')
//         await form[5].type('Blue')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//             await down[0].type('New')
//         await form[6].type('158-horse power')
//              await down[1].type('Manual')
//         // await form[7].type('Yes')
//              await down[2].type('Petron')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Transmission required.")
//          await (await browser).close()
//         }, 80000)

//--------------------Admin cannot add car without Fuel type--------------------//
// test('TC_Admin_21 Admin cannot add new car without Fuel type', async() => {

//     const browser = puppeteer.launch({headless: true, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')
//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('ToyotaCamry')
//         await form[2].type('Toyota')
//         await form[3].type('Camry')
//         await form[4].type('100000')
//         await form[5].type('Black')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//             await down[0].type('New')
//         await form[6].type('158-horse power')
//              await down[1].type('Manual')
//          await form[7].type('Yes')
//             // await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Fuel type required.")

//          await (await browser).close()
//         }, 80000)
//-------------ADMIN cannot add new car without details---------------------//
// test('TC_Admin_22 Admin cannot add new car without details', async() => {

//     const browser = puppeteer.launch({headless: true, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username','admin') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')
//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("VIN Required.")

//          await (await browser).close()
//         }, 80000)

//--------------- Admin can add new car with complete details ----OK-------------///
// test('TC_admin_23 Admin can add new car with complete details', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//     const form = await page.$$('.form-control')
//         await form[1].type('Hilux2010')
//         await form[2].type('Toyota')
//         await form[3].type('Hilux')
//         await form[4].type('100000')
//         await form[5].type('Pink')
//             const down = await page.$$('.mb-2')
//             await page.click('.mb-2')
//                 await down[0].type('New')
//             await form[6].type('70-horse power')    
//             await down[1].type('Manual')

//             await form[7].type('Yes')          
//              await down[2].type('Diesel')

//             await page.waitForSelector('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addCarModal___BV_modal_content_ > #addCarModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
//             await page.click('#app > .wrapper > div > div > .alert')


//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Cars has been added")

//          await (await browser).close()
//         }, 80000)

// ------------Admin can search a CAR----------ok--------//
// test('TC_admin_24 Admin can search car', async() =>{
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await(await page).setViewport({ width: 2133, height: 1076 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'AUDI')

//     await (await page).waitForSelector('.table.b-table.table-sm')
//        const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//         expect(html).toBeTruthy();
//     await (await browser).close()
// }, 50000)
  // await (await page).waitForSelector('.btn.btn-secondary') //i click ang clear button yes gurl
  //   await (await page).click('.btn.btn-secondary')


//-----------------------------Admin can update car details------Oks-----------//
// test('TC_admin_25 Admin can update employee details', async() => {

// browser = await puppeteer.launch({headless: false, slowMo: 100})
// page = await browser.newPage()
// await (await page).goto(url)
// await page.setViewport({ width: 1401, height: 928 })

// await (await page).waitForSelector('.username')
// await (await page).click('.username')

// await (await page).type('.username','admin') //ibutang niya ang ID

// await (await page).waitForSelector('.password')
// await (await page).click('.password')

// await (await page).type('.password', '1234')
// await (await page).click('.btn.login')

//     await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

// await (await page).waitForSelector('#filterInput')
// await (await page).click('#filterInput')
// await (await page).type('#filterInput', 'Audi');

// await page.waitForSelector('.btn.mr-1')
// await page.click('.btn.mr-1')
// const form = await page.$$('.form-control')
//     await form[2].click( { clickCount: 3 }) // i clear niya ang content
//     await form[2].type('Audi')

//     //Click submit button
//     await page.waitForSelector('.modal-dialog > #editCarModal___BV_modal_content_ > #editCarModal___BV_modal_body_ > form > .btn')
//     await page.click('.modal-dialog > #editCarModal___BV_modal_content_ > #editCarModal___BV_modal_body_ > form > .btn')
    
//     await page.waitForSelector('#app > .wrapper > div > div > .alert')

//     //Expect
//      const data = await page.evaluate(() => {
//          let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//          return Invalid
//      })
//      expect (data).toMatch('Cars has been updated')

//     await browser.close();   
// }, 500000)

//--------------------------- Repair services-------------------------------------//
//------- Admin can View repair list ---------Okay--------//


//  test ('TC_admin_26 Admin can view repair services list', async() => {
//       const browser = await puppeteer.launch({headless: false, slowMo: 100})
//       const page = await browser.newPage()

//       await (await page).goto(url)
//       await(await page).setViewport({ width: 2133, height: 1076 })

//       await (await page).waitForSelector('.username')
//       await (await page).click('.username')

//       await (await page).type('.username','admin') //ibutang niya ang ID

//       await (await page).waitForSelector('.password')
//       await (await page).click('.password')

//       await (await page).type('.password', '1234')
//       await (await page).click('.btn.login')

//       await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//       await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')

//     //   await page.waitForSelector('btn btn-primary');
//     //  await page.click('btn btn-primary')
//       await (await page).waitForSelector('.table.b-table.table-sm')
//           const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//           expect(html).toBeTruthy();
//      await browser.close();
//  }, 200000)



//----------------------- admin cannot add new repair service without Service name ---------- Ok-----//

// test('TC_Admin_27 Admin cannot add new repair services without Service Name', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//         const form = await page.$$('.form-control')
//             //  await form[1].type('Change color')
//              await form[2].type('Alignment of wheels')
//              await form[3].type('500')
//              await form[4].type('3')
//              await form[5].type('1')
     
//           //Create buttom
//             await page.waitForSelector('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Service Name required.")

//          await (await browser).close()
//         }, 80000)


//----------------------- admin cannot add new repair service without Price ---------- Ok-----//

// test('TC_admin_28 Admin cannot add new repair services without Price', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//         const form = await page.$$('.form-control')
//              await form[1].type('Change color')
//              await form[2].type('Alignment of wheels')
//             //  await form[3].type('500')
//              await form[4].type('3')
//              await form[5].type('1')
     
//           //Create buttom
//             await page.waitForSelector('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Empty price rate.")

//          await (await browser).close()
//         }, 80000)


//----------------------- admin cannot add new repair service without duration ---------- Ok-----//

// test('TC_Admin_29 Admin cannot add new repair services without duration', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//         const form = await page.$$('.form-control')
//              await form[1].type('Change color')
//              await form[2].type('Alignment of wheels')
//              await form[3].type('500')
//             //  await form[4].type('3')
//              await form[5].type('1')
     
//           //Create button
//             await page.waitForSelector('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Empty duration.")

//          await (await browser).close()
//         }, 80000)


//----------------------- admin cannot add new repair service without Mechanic Required ---------- Ok-----//

// test('TC_Admin_30 Admin cannot add new repair services without Mechanic Required', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//         const form = await page.$$('.form-control')
//              await form[1].type('Change color')
//              await form[2].type('Alignment of wheels')
//              await form[3].type('500')
//              await form[4].type('3')
//             //  await form[5].type('1')
     
//           //Create buttom
//             await page.waitForSelector('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Number of mechanics required.")

//          await (await browser).close()
//         }, 80000)


//----------------------- admin can add new repair service with complete details---------Ok------//

// test('TC_admin_31 Admin can add new repair Service', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await page.waitForSelector('.btn.btn-primary');
//     await page.click('.btn.btn-primary')
//         const form = await page.$$('.form-control')
//              await form[1].type('Sterring alignment')
//              await form[2].type('Alignment')
//              await form[3].type('10000')
//              await form[4].type('20')
//              await form[5].type('8')
     
//           //Create buttom
//             await page.waitForSelector('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')
//             await page.click('.modal-dialog > #addRepairModal___BV_modal_content_ > #addRepairModal___BV_modal_body_ > form > .btn')

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Repair Service has been added")

//          await (await browser).close()
//         }, 80000)

//------admin can search a repair service ----Ok--------//

// test('TC_admin_32 Admin can search a repai service', async() =>{
//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await(await page).setViewport({ width: 2133, height: 1076 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
    
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await (await page).waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')

//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Change oil')

//     await (await page).waitForSelector('.table.b-table.table-sm')
//        const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//         expect(html).toBeTruthy();
//     await (await browser).close()
// }, 50000)


///---- can update repair service ---Okay----//
// test('TC_admin_33 Admin can update a repair service', async() => {

//     const browser = puppeteer.launch({headless: false, slowMo: 50})
//     const page = await (await browser).newPage()
//     await (await page).goto(url)
//     await page.setViewport({ width: 1401, height: 928 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','admin') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     await page.waitForSelector('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).click('.navbar-nav > a:nth-child(3) > .nav-item > .nav-link > p')
//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Change')
//     await page.waitForSelector('#my-table > tbody > tr > td > .btn');
//     await page.click('#my-table > tbody > tr > td > .btn')
//         const form = await page.$$('.form-control')
//              await form[1].click( { clickCount: 3 }) // i clear niya ang content
//              await form[1].type('Change tire')
             
//             //  await form[2].type('Need repair tire')
//             //  await form[3].type('10000')
//             //  await form[4].type('20000')
//             //  await form[5].type('8000')
     

//            await page.waitForSelector('.modal-dialog > #editRepairModal___BV_modal_content_ > #editRepairModal___BV_modal_body_ > form > .btn')
//            await page.click('.modal-dialog > #editRepairModal___BV_modal_content_ > #editRepairModal___BV_modal_body_ > form > .btn')
  

//             await page.waitForSelector('#app > .wrapper > div > div > .alert')
           
//             const data = await page.evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Repair Service has been updated")

//          await (await browser).close()s
//         }, 80000)

