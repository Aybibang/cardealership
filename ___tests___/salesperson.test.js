const puppeteer = require('puppeteer');
let browser;
let page;
const url = 'http://172.16.4.182:7500/';

// ----------Salesperson can view list of orders -------------ok----------//

// test('TC_sales_01 Salesperson can view list of orders', async () => {
//     const browser = puppeteer.launch({
//         headless: false, slowMo: 50
//     })
//     const page = (await browser).newPage()

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')

//     //Wait for data to be displayed on table
//     await (await page).waitForSelector('.table.b-table.table-sm')
//     const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//     expect(html).toBeDefined();

//     await (await browser).close()
// }, 300000)

//---------------Salesperson can search an order ------okay----//

// test('TC_Sales_02 Admin can search an order')', async() =>{
//     const browser = puppeteer.launch({headless: false, slowMo: 100})
//     const page = (await browser).newPage()


//     await (await page).goto(url)
//     await(await page).setViewport({ width: 2133, height: 1076 })

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username','salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', '2020');
//     await (await page).waitForSelector('.table.b-table.table-sm')
//        const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//        expect(html).toBeTruthy();

//     // await (await page).goto('http://172.16.4.182:7500/adminmanage')
//     // await (await page).waitForSelector('.form-control')
//     await (await browser).close()

// }, 20000)


///////------------------------ Salesperson cannot create sales invoice without car order----------ok--------//

// test('TC_sales_03 salesperson cannot create new sales invoice without car order', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 100 })
//     const page = (await browser).newPage()
//     let input__customer; //Customer
//     let input__carorder; //Car Order

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')



//     const form = await (await page).$$('#bv-modal-example___BV_modal_content_')
//     const down = await (await page).$$('.mb-2.custom-select')
//                 await (await page).click('.mb-2.custom-select')
//                 await down[0].type("John Rey B Asong")
//                 await down[0].click()



//                 await (await page).waitForSelector('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')
//                 await (await page).click('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')

//                 await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//                 await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Service ticket Number required.")
//     await (await browser).close()
// }, 100000)

///////------------------------ Salesperson cannot create sales invoice without Customer-------Oki----------//
// test('TC_sales_04 salesperson cannot create new sales invoice without customer       ', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 100 })
//     const page = (await browser).newPage()
   

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')


//     const hrefs = await (await page).$$eval('.custom-select[id]', aTags => aTags.map(button => button.getAttribute("id")))
//     // console.log(hrefs)

//                 await (await page).click('#'+hrefs[1])
                   
//                     await (await page).type('#'+hrefs[1], 'Audi')
//                     await (await page).click('#'+hrefs[1])



//                 await (await page).waitForSelector('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')
//                 await (await page).click('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')

//                 await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//                 await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer ID required.")
//     await (await browser).close()
// }, 100000)

// //------------------------ Salesperson cannot create sales invoice without details--------ok---------//

// test('TC_sales_05 salesperson cannot create new sales invoice without details', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 100 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')



//     const form = await (await page).$$('#bv-modal-example___BV_modal_content_')
//     // const down = await (await page).$$('.mb-2.custom-select')
//     //             await (await page).click('.mb-2.custom-select')
//     //             await down[0].type()
//     //             await down[0].click()



//                 await (await page).waitForSelector('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')
//                 await (await page).click('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')

//                 await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//                 await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer ID required.")
//     await (await browser).close()
// }, 100000)

///////------------------------ Salesperson can create sales invoice with complete details and existing customer-------Oki----------//
// test('TC_Sales_06 salesperson can create new sales invoice with complete details and existing customer', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 100 })
//     const page = (await browser).newPage()
    

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')


//     const hrefs = await (await page).$$eval('.custom-select[id]', aTags => aTags.map(button => button.getAttribute("id")))
//     // console.log(hrefs)

//                 await (await page).click('#'+hrefs[0])
//                 await (await page).type('#'+hrefs[0], 'John')
//                 await (await page).click('#'+hrefs[0])
//                 await (await page).click('#'+hrefs[1])
                   
//                     await (await page).type('#'+hrefs[1], 'Audi')
//                     await (await page).click('#'+hrefs[1])



//                 await (await page).waitForSelector('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')
//                 await (await page).click('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')

//                 await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//                 await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer ID required.")
//     await (await browser).close()
// }, 100000)

//--------------Validation for Salesperson cannot add new customer without name-----------FAIL----------//
// test('TC_sales_07 salesperson cannot add new customer without name', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')


//     const down = await (await page).$$('.form-control')
//            // await down[1].type('Kurt')
//             //await down[2].type('S.')
//             //await down[3].type('Clear')
//             await down[4].type('kurt@gmail.com')
//             await down[5].type('09486264048')
                    

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer added.")
//             await (await browser).close()
// }, 100000)
//-----------------------sales person cannot add new employee without E-mail-----------------------------
// test('TC_sales_08 salesperson cannot add new customer without E-mail', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')


//     const down = await (await page).$$('.form-control')
//             await down[1].type('Kurt')
//             await down[2].type('S.')
//             await down[3].type('Clear')
//             // await down[4].type('kurt@gmail.com')
//             await down[5].type('09486264048')
                    

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer added.")
//             await (await browser).close()
// }, 100000)

//////////--------------Salesperson cannot add new customer without phone number-------------------------------//
// test('TC_sales_09 salesperson cannot add new customer without phone number', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')


//     const down = await (await page).$$('.form-control')
//            await down[1].type('Kurt')
//             await down[2].type('S.')
//             await down[3].type('Clear')
//             await down[4].type('kurt@gmail.com')
//             // await down[5].type('09486264048')
                    

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer added.")
//             await (await browser).close()
// }, 100000)

//----------------------salesperson cannot add new customer without details------------------------//
// test('TC_sales_10 salesperson cannot add new customer without name', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')


//     const down = await (await page).$$('.form-control')
//            // await down[1].type('Kurt')
//             //await down[2].type('S.')
//             //await down[3].type('Clear')
//             // await down[4].type('kurt@gmail.com')
//             // await down[5].type('09486264048')
                    

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Name required.")
//             await (await browser).close()
// }, 100000)


//------------------------ Salesperson can add new customer--------ok- wala pa sa test script------//
// test('TC_sales_10 salesperson can add new customer with complete details', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')


//     const down = await (await page).$$('.form-control')
//             await down[1].type('Kurt')
//             await down[2].type('S.')
//             await down[3].type('Clear')
//             await down[4].type('kurt@gmail.com')
//             await down[5].type('09486264048')
                    

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Customer added.")
//             await (await browser).close()
// }, 100000)


//-------------------Salesperson can add new sales invoice with new customer------change data hehhehe///////////
// test('TC_sales_12 salesperson can add new invoice with new customer', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  

//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('#show-btn')
//     await (await page).click('#show-btn')

//     await (await page).waitForSelector('.btn.add-customer.btn-secondary')
//     await (await page).click('.btn.add-customer.btn-secondary')

//     const down = await (await page).$$('.form-control')
//             await down[1].type('Ivy')
//             await down[2].type('S.')
//             await down[3].type('Clear')
//             await down[4].type('ivy@gmail.com')
//             await down[5].type('09486264048')

//             await (await page).waitForSelector('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
//             await (await page).click('#add-customer-modal___BV_modal_content_ > #add-customer-modal___BV_modal_body_ > .d-block > form > .btn')
                    

//     const hrefs = await (await page).$$eval('.custom-select[id]', aTags => aTags.map(button => button.getAttribute("id")))
//     // console.log(hrefs)

//                 await (await page).click('#'+hrefs[0])
//                 await (await page).type('#'+hrefs[0], 'John')
//                 await (await page).click('#'+hrefs[0])
//                 await (await page).click('#'+hrefs[1])
                   
//                     await (await page).type('#'+hrefs[1], 'Audi')
//                     await (await page).click('#'+hrefs[1])


//                 await (await page).waitForSelector('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')
//                 await (await page).click('#bv-modal-example___BV_modal_content_ > #bv-modal-example___BV_modal_body_ > .d-block > form > .btn')

//             await (await page).waitForSelector('#app > .wrapper > div > div > .alert')
//             await (await page).click('#app > .wrapper > div > div > .alert')


//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Sales invoice made.")
//             await (await browser).close()
// }, 100000)


//-----------sales person can edit an existing orders from the list --------------Ok-----------------------//
// test('TC_sales_13 salesperson can edit existing order from the list', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  
// //log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)

//     await (await page).waitForSelector('.btn.mr-1.btn-info.btn-sm')
//     await (await page).click('.btn.mr-1.btn-info.btn-sm')

//     const form = await (await page).$$('.form-control')
//         await form[1].type('11192020')
//         await form[2].type('')

//         await (await page).waitForSelector('#edit-modal___BV_modal_content_ > #edit-modal___BV_modal_body_ > .d-block > form > .btn')
//         await (await page).click('#edit-modal___BV_modal_content_ > #edit-modal___BV_modal_body_ > .d-block > form > .btn')

//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Order updated.")
//     await (await browser).close()
// }, 200000)


//------------- Salesperson can create sales invoice from existing order ------Ok----change ang data-----------------//
// test('TC_sales_14 salesperson can create sales invoice from the list', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  
// //log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


//     //Search invoice
//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Asong');
//     await (await page).waitForSelector('.btn.mr-1.btn-success.btn-sm')
//     await (await page).click('.btn.mr-1.btn-success.btn-sm')

//     //Create sales invoice button
//     await (await page).waitForSelector('.modal-dialog > #salesinvoice-modal___BV_modal_content_ > #salesinvoice-modal___BV_modal_body_ > .d-block > .btn')
//     await (await page).click('.modal-dialog > #salesinvoice-modal___BV_modal_content_ > #salesinvoice-modal___BV_modal_body_ > .d-block > .btn')
  

//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Sales invoice made.")
//     await (await browser).close()
// }, 200000)

//------------- Salesperson can cancel existing order ------Ok----change ang data-----------------//
// test('TC_salesperson_15 salesperson can cancel existing order', async () => {
//     const browser = puppeteer.launch({ headless: false, slowMo: 50 })
//     const page = (await browser).newPage()
    
//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })
  
// //log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')

//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID

//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')

//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login', 30)


//     //Search invoice
//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Asong');
//     await (await page).waitForSelector('.btn.mr-1.btn-danger.btn-sm')
//     await (await page).click('.btn.mr-1.btn-danger.btn-sm')

//     //Cancel
//     await (await page).waitForSelector('.modal-dialog > #delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .btn')
//     await (await page).click('.modal-dialog > #delete-modal___BV_modal_content_ > #delete-modal___BV_modal_body_ > .d-block > .btn')
    
  

//             const data = await (await page).evaluate(() => {
//                   let Invalid = document.querySelector('#app > .wrapper > div > div > .alert').innerText;
//                          return Invalid;})
//                     expect(data).toMatch("Order canceled.")
//     await (await browser).close()
// }, 200000)

//--------------------------------------------------Manage sales invoice----------------------------------------------//


//-------------- sales person can view list of sales invoice---------//

// test('TC_sales_16 Salesperson can view list of existing sales invoice', async () => {
//         const browser = puppeteer.launch({
//             headless: false, slowMo: 50
//         })
//         const page = (await browser).newPage()
    
//         await (await page).goto(url)
//         await (await page).setViewport({ width: 1401, height: 928 })
    
//         //Log in
//         await (await page).waitForSelector('.username')
//         await (await page).click('.username')
//         await (await page).type('.username', 'salesperson') //ibutang niya ang ID
//         await (await page).waitForSelector('.password')
//         await (await page).click('.password')
//         await (await page).type('.password', '1234')
//         await (await page).click('.btn.login')


//       await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//      await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//         //Wait for data to be displayed on table
//         await (await page).waitForSelector('.table.b-table.table-sm')
//         const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//         expect(html).toBeDefined();
    
//         await (await browser).close()
//     }, 300000)

///-------------salesperson can view list of existing sales invoice-----------------//
// test('TC_sales_17 Salesperson can search list of existing sales invoice', async () => {
//     const browser = puppeteer.launch({
//         headless: false, slowMo: 50
//     })
//     const page = (await browser).newPage()

//     await (await page).goto(url)
//     await (await page).setViewport({ width: 1401, height: 928 })

//     //Log in
//     await (await page).waitForSelector('.username')
//     await (await page).click('.username')
//     await (await page).type('.username', 'salesperson') //ibutang niya ang ID
//     await (await page).waitForSelector('.password')
//     await (await page).click('.password')
//     await (await page).type('.password', '1234')
//     await (await page).click('.btn.login')


//   await (await page).waitForSelector('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')
//  await (await page).click('.navbar-nav > a:nth-child(2) > .nav-item > .nav-link > p')

//     await (await page).waitForSelector('#filterInput')
//     await (await page).click('#filterInput')
//     await (await page).type('#filterInput', 'Rey');

//     //Wait for data to be displayed on table
//     await (await page).waitForSelector('.table.b-table.table-sm')
//     const html = await (await page).$eval('.table.b-table.table-sm', text => text.innerHTML);
//     expect(html).toBeMatch('Rey');

//     await (await browser).close()
// }, 300000)